Do you want to set up your own Jekyll static-generated site like this one? 

To create a Jekyll static-generated website like this one, you will need the following:

* Git
* Ruby
* A Gitlab account

View the tutorial [here](tutorial_index.md) for more info.
